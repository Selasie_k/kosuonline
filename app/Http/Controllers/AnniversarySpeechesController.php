<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AnniversarySpeechesController extends Controller
{
    public function show($speech)
    {
        return view("pages.anniversary.$speech");
    }
}
