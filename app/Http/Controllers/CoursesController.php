<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CoursesController extends Controller
{
    public function show($page)
    {
        $course = collect(config('kasec.courses'))->firstWhere('page', $page);

        abort_if(!$course, 404);

        return view("pages.courses.{$course['page']}", ['course' => $course]);
    }
}
