<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function academics()
    {
        return view('pages.academics');
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function alumini()
    {
        return view('pages.alumini');
    }

    public function gallery()
    {
        return view('pages.gallery');
    }

    public function history()
    {
        return view('pages.history');
    }

    public function execProfilePage($name)
    {
        $exec = collect(config('kasec.kosu.executives'))->firstWhere('name', $name);
        if (!$exec) {
            abort(404);
        }
        return view('pages.exec-profile', ['exec' => $exec]);
    }
}
