<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/history', 'HomeController@history')->name('history');
Route::get('/academics', 'HomeController@academics')->name('academics');
Route::get('/alumini', 'HomeController@alumini')->name('alumini');

Route::get('/management', 'ManagementController@index')->name('management');

Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/gallery', 'HomeController@gallery')->name('gallery');

Route::get('/alumini/executive/{name}', 'HomeController@execProfilePage')->name('executive-profile');

Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::prefix('/admin')->middleware('admin')->group(function () {
        Route::get('/dashboard', 'AdminDashboardController@index')->name('admin-dashboard');
    });
});


Route::get('courses/{page}', 'CoursesController@show')->name('courses');

Route::get('anniversary/speechs/{speech}', 'AnniversarySpeechesController@show')->name('anniversary.speech');