<?php

return [
    'kosu' => [
        'executives' => [
            [
                'name' => 'MICHAEL OTENG',
                'position' => 'KOSU PRESIDENT',
                'image' => '/images/kosu/president.jpg',
                'profile' => "
                        I entered KASEC in September, 1980 and completed sixth form in June 1987 after seven years of studies. <br><br>

                        ACADEMIC QUALIFICATIONS <br><br>

                        A.	2 MASTERS DEGREE <br>
                        •	MASTERS IN INTERNATIONAL MARITIME LAW (LLM) – UNITED KINGDOM <br>
                        •	MASTERS IN BUSINESS ADMINISTRATION – FINANCE OPTION (UNIVERSITY OF GHANA) <br><br>
                        
                        B.	POST GRADUATE DIPLOMA IN PORTS AND SHIPPING ADMINISTRATION (REGIONAL MARITIME ACADEMY/UNIVERSITY OF GHANA) <br><br>
                        
                        C.	 2 FIRST DEGREES <br>
                        •	BACHELOR OF LAWS (LLB) (UNIVERSITY OF GHANA) <br>
                        •	BACHELOR OF ARTS IN SOCIOLOGY (UNIVERSITY OF GHANA) <br><br>
                        
                        D.	PROFESSIONAL QUALIFYING CERTIFICATE IN LAW ( GHANA SCHOOL OF LAW-ACCRA) <br><br>
                        
                        E.	OTHER QUALIFICATIONS <br>
                        GHANA STOCK EXCHANGE SECURITIES COURSE (3 PAPERS WRITTEN & PASSED) <br>		
                        •	FUNDAMENTALS OF FINANCE AND FINANCIAL MARKETS <br>
                        •	INVESTMENT ADVISORY TECHNIQUES <br>
                        •	CORPORATE FINANCE <br><br>
                        
                        PROFESSIONAL AFFILIATIONS <br>
                        MEMBER, BRITISH INSURANCE LAW ASSOCIATION (BILA) - UNITED KINGDOM <br>
                        MEMBER, GHANA BAR ASSOCIATION (GBA) - GHANA <br><br>
                        
                        RELEVANT WORK EXPERIENCE <br>
                        •	WORKED AS INTERNAL AUDITOR AT VARIOUS MINISTRIES, DEPARTMENTS AND AGENCIES IN ACCRA FOR OVER 20 YEARS. <br>
                        •	CURRENTLY THE MANAGING PARTNER AND LAWYER AT LEX LARUTISS LAW FIRM IN TEMA COMMUNITY 9.
                ",
            ],
            [
                'name' => 'TASAH SAFIWU',
                'position' => 'KOSU VICE PRESIDENT',
                'image' => '/images/kosu/veep.jpeg',
                'profile' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae sunt laudantium nisi aspernatur unde. Ea hic ducimus debitis sint possimus...',
            ],
            [
                'name' => 'STEPHEN MORDEY',
                'position' => 'KOSU PRO',
                'image' => '/images/kosu/pro.jpg',
                'profile' => "Mordey Stephen Completed KASEC in 2009 and doubled as the Senior Prefect for the 2008/2009 Academic Year. <br> <br>
                        In 2010 he gained admission to study B.Ed. Social Sciences (Economics & Mathematics) at the University of Cape Coast. <br> <br>
                        In 2011 he championed and founded KOSU-University of Cape Coast Chapter and was President of the Union until 2014. <br> <br>
                        After a successful completion of his bachelor's degree program he joined Unibank Ghana Limited as a National Service Person in 2014. <br> <br>
                        He is currently a Custodian Banker with Consolidated Bank Ghana Limited and is Pursuing MSc. Economics with Kwame Nkrumah University of Science & Technology, IDL-Accra.
                    ",
            ],
            [
                'name' => 'EUNICE BOATENG',
                'position' => 'KOSU SECRETARY',
                'image' => '/images/avatar-female.png',
                'profile' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae sunt laudantium nisi aspernatur unde. Ea hic ducimus debitis sint possimus...',
            ],
            [
                'name' => 'DELIGHT ATSYOR',
                'position' => 'ASST. KOSU SECRETARY',
                'image' => '/images/avatar-female.png',
                'profile' => "Miss Sedinam Delight Atsyor is my name, a teacher by profession and the chaperone for Miss Tourism Ghana organization. 
                            I’m also the founder of impact Sedi foundation  aimed at creating positive impacts to the society for the love of humanity. 
                            A proud member of the Great Kasec Old school union and ready to serve my alma mater to the best of my ability. <br><br>
                            I see life as a learning opportunity and as a reminder to myself, hopes for a better future is not enough. 
                            That one must be an active participant in the quality of life, if I want flowers in the future, the planting must be done now. <br><br>
                            I’m opened to new ideas and interested in bussiness ventures. I love to read, write, travel and listening to good music.
                ",
            ],
        ]
    ],

    'courses' => [
        [
            'name' => 'General Science',
            'image' => '/images/courses/chemistry.png',
            'page' => 'general-science',
        ],
        [
            'name' => 'Business',
            'image' => '/images/courses/analytics.png',
            'page' => 'business',
        ],
        [
            'name' => 'Social Sciences',
            'image' => '/images/courses/balance.png',
            'page' => 'social-sciences',
        ],
        [
            'name' => 'Vocational Department',
            'image' => '/images/courses/cooking.png',
            'page' => 'vocational',
        ],
        [
            'name' => 'Agricultural Science',
            'image' => '/images/courses/planting.png',
            'page' => 'agric-science',
        ],
        // [
        //     'name' => 'Visual Arts',
        //     'image' => '/images/courses/paint-palette.png',
        //     'slug' => 'general-science',
        // ],
    ],

    'news' => [
        [
            'name' => '60th Anniversary',
            'image' => '/images/logo-60.jpeg',
        ],
        [
            'name' => 'Speech & Prize',
            'image' => '/images/admin2.jpeg',
        ],
    ],

    'speeches' => [
        [
            'title' => 'SPEECH DELIVERED BY DR. GIDEON TAY, THE HEADMASTER OF KADJEBI-ASATO SECONDARY SCHOOL.',
            'image' => "/images/anniversary/gideon-tay.jpeg",
            'uri' => 'headmasters-speech'
        ],
        [
            'title' => 'ADDRESS DELIVERED ON BEHALF OF THE VICE PRESIDENT OF THE REPUBLIC OF GHANA BY  HON. BONIFACE ABUBAKAR SADDIQUE',
            'image' => "/images/anniversary/veep-rep.jpg",
            'uri' => 'veep-speech'
        ],
        [
            'title' => 'SPECIAL ANNIVERSARY MESSAGE FROM THE DEPUTY MINISTER, GENERAL EDUCATION AND MP FOR BOSOMTWE HON. DR. YAW OSEI ADUTWUM',
            'image' => "/images/anniversary/education-minister.jpg",
            'uri' => 'education-minister'
        ],
        [
            'title' => 'SPEECH DELIVERED BY MR. MICHAEL KWASI OTENG, PRESIDENT OF KASEC OLD STUDENTS UNION',
            'image' => "/images/anniversary/kosu-president.jpg",
            'uri' => 'kosu-president'
        ],
    ],
];
