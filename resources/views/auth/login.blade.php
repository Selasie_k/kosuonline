<x-app-layout>
    <div class="h-screen bg-gray-300 flex items-center">
        <div class="w-full px-4">
            <div class="max-w-sm mx-auto bg-white p-4 rounded-lg shadow">
                <p class="text-center text-2xl text-gray-700">Login</p>
                <form method="POST" action="{{ route('login') }}">
                    @csrf
        
                    <form-input label="Email" name="email" value="{{ old('email') }}" @error('email') error="{{ $message }}" @enderror required></form-input>
        
                    <form-input label="Password" type="password" name="password" @error('password') error="{{ $message }}" @enderror required></form-input>

                    <div class="mt-3">
                        <label class="text-gray-700" for="remember">
                            <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            Remember me
                        </label>
                    </div>

                    <button class="block w-full mt-3 py-3 bg-gray-800 text-white rounded hover:bg-gray-900">Login</button>
        
                    @if (Route::has('password.request'))
                        <div class="mt-3 text-center">
                            <a class="text-gray-700 text-sm" href="{{ route('password.request') }}">
                                Forgot Your Password?
                            </a>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>    
</x-app-layout>
