<x-app-layout>
    <div class="min-h-screen bg-gray-300 flex items-center py-16">
        <div class="w-full px-4">
            <form method="POST" action="{{ route('register') }}">
            
                <div class="max-w-sm mx-auto">

                    <p class="text-center text-2xl text-gray-700">Register</p>
                    @csrf

                    <div class="bg-white p-4 rounded-lg shadow mt-4">
                        <form-input label="First Name"  name="first_name"   value="{{ old('first_name') }}"     @error('first_name') error="{{ $message }}" @enderror   required></form-input>
                        <form-input label="Last Name"   name="last_name"    value="{{ old('last_name') }}"      @error('last_name') error="{{ $message }}" @enderror    required></form-input>
                    </div>

                    <div class="bg-white p-4 rounded-lg shadow mt-4">
                        <form-input label="Email"       name="email"        value="{{ old('email') }}"          @error('email') error="{{ $message }}" @enderror        required></form-input>
                        <form-input label="Phone"       name="phone"        value="{{ old('phone') }}"          @error('phone') error="{{ $message }}" @enderror        required></form-input>
                    </div>

                    <div class="bg-white p-4 rounded-lg shadow mt-4">
                        <form-input label="Year Group"  name="year_group"   value="{{ old('year_group') }}"     @error('year_group') error="{{ $message }}" @enderror   required></form-input>
                        <form-input label="Country"     name="country"      value="{{ old('country') }}"        @error('country') error="{{ $message }}" @enderror      required></form-input>
                        <form-input label="Occupation"  name="occupation"   value="{{ old('occupation') }}"     @error('occupation') error="{{ $message }}" @enderror   required></form-input>
                        <form-input label="Company"     name="company"      value="{{ old('company') }}"        @error('company') error="{{ $message }}" @enderror      required></form-input>
                    </div>
        
                    <div class="bg-white p-4 rounded-lg shadow mt-4">
                        <form-input label="Password"    name="password" type="password"    value="{{ old('password') }}"       @error('password') error="{{ $message }}" @enderror     required></form-input>
                        <form-input label="Confirm Password" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}" @error('password_confirmation') error="{{ $message }}" @enderror required></form-input>
                    </div>

                    <button class="block w-full mt-6 py-3 bg-gray-800 text-white rounded hover:bg-gray-900">Register</button>

                </div>
            </form>
            
        </div>
    </div>
</x-app-layout>
