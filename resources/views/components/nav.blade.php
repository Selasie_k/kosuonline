<nav class="bg-gray-900 text-gray-200" x-data="{ open: false }">
    <div class="flex justify-between items-center p-4 max-w-screen-lg mx-auto">

        <a class="tracking-wider" href="{{ url('/') }}">
            <span class="text-orange-400">KASEC</span><span class="text-gray-400">ONLINE</span> 
        </a>
        <div class="sm:hidden">
            <button class="block focus:outline-none" x-on:click="open = !open">
                <svg x-show="open" width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><path d="M18 6L6 18M6 6l12 12"/></svg>
                <svg x-show="!open"width="24" height="24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><path d="M3 12h18M3 6h18M3 18h18"/></svg>
            </button>
        </div>

        {{-- large screen nav links --}}
        <div class="hidden sm:flex space-x-3">
            <a href="/" class="hover:text-yellow-200 {{request()->is('/') ? 'text-yellow-200' : 'text-gray-500'}}">Home</span></a>

            <a href="/history" class="hover:text-yellow-200 {{request()->is('history') ? 'text-yellow-200' : 'text-gray-500'}}">History</span></a>

            <a href="/#courses-offered" class="hover:text-yellow-200 {{request()->is('academics') ? 'text-yellow-200' : 'text-gray-500'}}">Academics</span></a>
            {{-- <a href="/academics" class="hover:text-yellow-200 {{request()->is('academics') ? 'text-yellow-200' : 'text-gray-500'}}">Academics</span></a> --}}
            
            <a href="/management" class="hover:text-yellow-200 {{request()->is('management') ? 'text-yellow-200' : 'text-gray-500'}}">Management</span></a>

            <a href="/alumini" class="hover:text-yellow-200 {{request()->is('alumini*') ? 'text-yellow-200' : 'text-gray-500'}}">Alumini</span></a>

            {{-- <a href="/contact" class="hover:text-yellow-200 {{request()->is('contact') ? 'text-yellow-200' : 'text-gray-500'}}">Contact Us</span></a> --}}

            {{-- <a href="/gallery" class="hover:text-yellow-200 {{request()->is('gallery') ? 'text-yellow-200' : 'text-gray-500'}}">Gallery</span></a>

            <a href="{{route('admin-dashboard')}}" class="text-gray-500 hover:text-yellow-200">Events</span></a> --}}

            {{-- @guest
                <a class="border-b-2 border-gray-900 hover:border-orange-500" href="{{ route('login') }}">Login</a>

                <a class="border-b-2 border-gray-900 hover:border-orange-500" href="{{ route('register') }}">Register</a>
            @else

                <a href="{{route('admin-dashboard')}}" class="text-gray-500 hover:text-yellow-200">Admin</span></a>

                <a class="border-b-2 border-gray-900 hover:border-orange-500" href="#">{{ Auth::user()->name }}</span></a>

                <a class="text-gray-700 hover:text-red-500" href="{{ route('logout') }}"
                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <app-icon name="lock" :size="4"></app-icon>
                    Logout
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest --}}
        </div>
    </div>

    {{-- mobile nav links --}}
    <div class="px-4 pb-4 flex flex-col space-y-3 sm:hidden" x-show.transition="open">

        <a href="/" class="hover:text-yellow-200 {{request()->is('/') ? 'text-yellow-200' : 'text-gray-500'}}">Home</span></a>

        <a href="/history" class="hover:text-yellow-200 {{request()->is('history') ? 'text-yellow-200' : 'text-gray-500'}}">History</span></a>

        <a href="/#courses-offered" class="hover:text-yellow-200 {{request()->is('academics') ? 'text-yellow-200' : 'text-gray-500'}}">Academics</span></a>
        {{-- <a href="/academics" class="hover:text-yellow-200 {{request()->is('academics') ? 'text-yellow-200' : 'text-gray-500'}}">Academics</span></a> --}}

        <a href="/management" class="hover:text-yellow-200 {{request()->is('management') ? 'text-yellow-200' : 'text-gray-500'}}">Management</span></a>

        <a href="/alumini" class="hover:text-yellow-200 {{request()->is('alumini') ? 'text-yellow-200' : 'text-gray-500'}}">Alumini</span></a>

        {{-- <a href="/contact" class="hover:text-yellow-200 {{request()->is('contact') ? 'text-yellow-200' : 'text-gray-500'}}">Contact Us</span></a> --}}

        {{-- @guest
            <a href="{{ route('login') }}">Login</a>

            @if (Route::has('register'))
                <a href="{{ route('register') }}">Register</a>
            @endif
        @else
            <a href="{{route('admin-dashboard')}}" class="text-white">Dashboard</span></a>

            <a href="#" class="text-white">{{ Auth::user()->name }}</span></a>

            <a class="text-gray-400 hover:text-red-500" href="{{ route('logout') }}"
                onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <app-icon name="lock" :size="4"></app-icon>
                Logout
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        @endguest --}}
    </div>
</nav>