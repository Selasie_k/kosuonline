<x-app-layout>
    <meta name="viewport" content="width=1024">
    <div class="max-w-screen-lg mx-auto py-10 h-screen">
        <div class="flex">
            <div class="">
                <ul class="text-gray-700 capitalize space-y-6">
                    <li class="">
                        <a href="#" class="border border-white hover:border-gray-700 py-2 px-3 block hover:text-gray-900">
                            <app-icon name="pie-chart" :size="4"></app-icon> Dashboard
                        </a>
                    </li>
                    <li class="">
                        <a href="#" class="border border-white hover:border-gray-700 py-2 px-3 block hover:text-gray-900">
                            <app-icon name="users" :size="4"></app-icon> Members
                        </a>
                    </li>
                    <li class="">
                        <a href="#" class="border border-white hover:border-gray-700 py-2 px-3 block hover:text-gray-900">
                            <app-icon name="award" :size="4"></app-icon> Year Groups
                        </a>
                    </li>
                    <li class="">
                        <a href="#" class="border border-white hover:border-gray-700 py-2 px-3 block hover:text-gray-900">
                                <app-icon name="file-text" :size="4"></app-icon> Contributions
                        </a>
                    </li>
                    <li class="">
                        <a href="#" class="border border-white hover:border-gray-700 py-2 px-3 block hover:text-gray-900">
                            <app-icon name="bookmark" :size="4"></app-icon> Events
                        </a>
                    </li>
                    <li class="">
                        <a href="#" class="border border-white hover:border-gray-700 py-2 px-3 block hover:text-gray-900">
                            <app-icon name="cast" :size="4"></app-icon> News
                        </a>
                    </li>
                </ul>
            </div>
            <div class="flex-1 mx-12 px-4 border">
                {{$slot}}
            </div>
        </div>
    </div>
</x-app-layout>
