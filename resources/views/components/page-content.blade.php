<x-app-layout>
    <div class="py-12 bg-gray-800 text-white">
        <div class="container">
            <span class="text-2xl font-bold uppercase tracking-widest">{{$title}}</span> 
        </div>
    </div>
    <div class="container py-12">

        {{$slot}}
        
    </div>
</x-app-layout>