<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <x-badge-strip></x-badge-strip>
        <x-nav></x-nav>
        <main>
            {{$slot}}
        </main>
        <x-footer></x-footer>
    </div>

    <script>
        function initMap() {
            var kasec = {lat: 7.5376098, lng: 0.4629706};
            var map = new google.maps.Map(document.getElementById('map'), {zoom: 14, center: kasec});
            var marker = new google.maps.Marker({position: kasec, map: map});
        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Oaaq4ud1ZEQXowe5N6HGGS7BrmzOWCg&callback=initMap" async defer></script>
</body>
</html>
