<div class="bg-yellow-200">
    <div class="container py-4">
        <div class="flex items-center space-x-6">
            <div class="p-4 rounded-lg bg-red-800 inline-block">
                <a href="/">
                    <img src="/images/badge.jpeg" alt="badge" class="h-16">
                </a>
            </div>
            <div>
                <a href="/" class="hover:underline">
                    <p class="sm:text-2xl font-bold text-red-800">KADJEBI-ASATO</p>
                    <p class="text-red-700 tracking-widest">SENIOR HIGH SCHOOL</p>
                </a>
            </div>
        </div>
    </div>
</div>