<footer class="py-16 bg-black">
    <div class="max-w-screen-lg mx-auto px-4">
        <div class="sm:flex items-start">
            {{-- <div class="flex justify-center mb-6">
                <div class="bg-white rounded inline-block">
                    <div class="p-4 rounded">
                        <img src="/images/badge.jpeg" alt="badge" class="h-32 mx-auto">
                    </div>
                    <div class="bg-gray-300 p-4 rounded text-center">
                        <p>Kadjebi-Asato</p>
                        <p>Senior High School</p>
                    </div>
                </div>
            </div> --}}

            <div class="flex mb-6">
                <div class="text-gray-600 px-4 mt-4 sm:mt-0 sm:ml-8">
                    <p class="text-gray-300 uppercase">About The School</p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">Our History</a></p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">Academics</a></p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">Contact Us</a></p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">Alumini</a></p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">Gallery</a></p>
                </div>
    
                <div class="text-gray-600 px-4 mt-4 sm:mt-0 sm:ml-8">
                    <p class="text-gray-300 uppercase">contact us</p>
                    {{-- <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">Contact page</a></p> --}}
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">address</a></p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">phone</a></p>
                    <p class="text-gray-500 text-sm my-2"><a href="#" class="hover:text-white hover:underline">email</a></p>
                </div>
            </div>
            <div class="flex-1 px-4 mb-6">
                <div id="map" class="h-64 border"></div>
            </div>
        </div>
    </div>
</footer>