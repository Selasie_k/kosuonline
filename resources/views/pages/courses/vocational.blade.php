<x-page-content>
    <x-slot name="title">
        The Vocational Department
    </x-slot>
    <div>
        <h1 class="font-bold mb-6">PROFILE OF  HEAD OF DEPARTMENT</h1>
        <div class="text-justify mb-6">
            <div class="mb-2">
                <img src="/images/hods/vocational.png" class="h-48 rounded-lg mb-4" alt="hod of business dept">
                <b>Vida Gbortsui</b> born on the 10th day of October, 1967, who successfully completed University College of Education Wineba 
                on the 7th Day of July, 2007 and was posted to Kadjebi-Asato Senior High School and remained as a teacher till date.
            </div>
        </div>

        <h1 class="font-bold mb-6">HIGHLIGHT OF DEPARTMENTAL ACTIVITIES</h1>
        <div class="text-justify mb-12">
            <span class="mb-2 block">
                The Vocational department is a combination of the Home Economics Department and Visual Art Department. It has over the years, 
                produced a number of students that are working in the job market with requisite skills acquired during their years of study in KASEC.
            </span>
            <span class="mb-2 block">
                The subjects offered in the department are:
            </span>

            <h2 class="font-bold block">For Home Economics- courses includes</h2> 
            <ul class="ml-6 mb-4">
                <li class="list-disc">Management In Living</li>
                <li class="list-disc">Foods & Nutrition</li>
                <li class="list-disc">Clothing & Textiles</li>
                <li class="list-disc">G.K.A/ French</li>
                <li class="list-disc">Biology as elective subjects.</li>
            </ul>

            <h2 class="font-bold block">For Visual Arts Department- courses includes</h2> 
            <ul class="ml-6 mb-4">
                <li class="list-disc">Graphic Design</li>
                <li class="list-disc">Basketry</li>
                <li class="list-disc">G.K.A</li>
                <li class="list-disc">Elective ICT / French</li>
            </ul>

            <h2 class="font-bold block my-4">Departmental Job Opportunities</h2> 

            <h2 class="font-bold block">Home Economics:</h2> 
            <ul class="ml-6 mb-4">
                <li class="list-disc">Food Technologist</li>
                <li class="list-disc">Dietician and their assistant</li>
                <li class="list-disc">Nutritionist</li>
                <li class="list-disc">Teaching</li>
                <li class="list-disc">Entrepreneur</li>
                <li class="list-disc">Food Service and their assistant</li>
                <li class="list-disc">Money Management</li>
                <li class="list-disc">Housing Carriers</li>
                <li class="list-disc">Clothing Designer</li>
                <li class="list-disc">Interior Decoration and many more</li>
            </ul>

            <h2 class="font-bold block">Visual Arts:</h2> 
            <ul class="ml-6 mb-4">
                <li class="list-disc">Cinematographer</li>  
                <li class="list-disc">Sign writer</li>  
                <li class="list-disc">Drawing </li> 
                <li class="list-disc">Painting</li> 
                <li class="list-disc">Leather works</li>    
                <li class="list-disc">Interior  and Exterior Decoration</li>    
            </ul>
            <span class="mb-2 block">
                I wish to intimate that the department is a promising one that will always see to the betterment of our dear students. 
            </span>
        </div>
    </div>
</x-page-content>