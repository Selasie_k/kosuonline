<x-page-content>
    <x-slot name="title">
        The Science Department
    </x-slot>
    <div>
        <h1 class="font-bold mb-6">PROFILE OF  HEAD OF DEPARTMENT</h1>
        <div class="text-justify mb-6">
            <span class="mb-2 block">
                Klu K John has been a teaching staff of Kadjebi-Asato Senior High School, since, 
                November, 2005-beginning as a subject instructor in Chemistry and Mathematics 
                and has since ascended the various timely academic ranks of the educational service. 
            </span>
            <span class="mb-2 block">
                He currently, is the Head of the Departement overseeing the operations of five sub-departments, 
                viz Mathematics, Chemistry, Physics, Biology and ICT.
            </span>
            <span class="mb-2 block">
                He holds a Bachelor of Science honours in Agricultural Engineering, (BSc. Hons Agric. Eng) and a 
                Postgraduate Diploma In Education (PGDE).
            </span>
            <span class="mb-2 block">
                He is an astute tutor in his area of tution, has massive knowlegde in science related fields having 
                participated in a series of science workshops and trainings over the years. He  is an affable fellow 
                who inspires teachable students- no matter how low their IQ’s to realize their dreams in life. 
            </span>

            <span class="mb-2 block">
                He was the first departmental staff to lead students of the department to the Schools maiden participation 
                in the National Science and Maths Quiz in the year 2008 and has since been activiely involved to date.
            </span>
            <span class="mb-2 block">
                In his contributions to extracurricular activities over the years in the school, he has served as the 
                Deputy House Parent of Akwafo House and The Substantive House master of Addo - Yobo House and Volta House. 
                He is also the current Chaplain of the School.
            </span>
        </div>

        <h1 class="font-bold mb-6">HIGHLIGHT ON ACTIVITIES OF THE DEPARTMENT</h1>
        <div class="text-justify mb-12">
            <span class="mb-2 block">
                This department ranks as the largest in the school and has its activities extending to all other departments in the school.
            </span>
            <span class="mb-2 block">
                It is structurally positioned with five sub departments namely Mathematics, Physics, Chemistry, Biology and ICT 
                that carry out its mandate of enlightening students’ mind with science theories, science concepts and their 
                applications to daily life and for human developement.
            </span>
            <span class="mb-2 block">
                In pursuant of this mandate, the department has incubated and hatched outstanding personnels in some remarkable 
                professional fields in the country. This was done via excellent delivery from the staff of the department in 
                the areas of pure and applied sciences.
            </span>
            <span class="mb-2 block">
                Science, therefore as a noble programme, offers job opportunites  to trainees in  various carreier paths-some of 
                which have direct associaations with science whiles others  are indirects offshoots of the science programmes. 
                To mention but a few, students pusuing sicence programme end up engageing  in carrers such as 
            </span>
            <ul class="ml-6 my-4">
                <li class="list-disc">
                    <span class="mb-2 block">
                       <span class="font-bold block">For Direct Associations:</span> 
                       Electrical engineering, Agricultural Engineer, Civil, Engineers, etc. Others are  Nursing, Human Biologist, 
                       Dentist, Laboratory technician, Food Scientist, Chemists, Physicist, Biologist, Research Scientists, metallurgist, 
                       Geophysicist, surveyors, Pharmaceutical Developement Sales, Research scientist, Secondary school teacher, etc
                    </span>
                </li>
                <li class="list-disc">
                    <span class="mb-2 block">
                       <span class="font-bold block">And For Indirect Associations:</span> 
                       carrer paths include Politician, Media Correspondent, Lawyer, Scientific writer, UN Representative, Publisher, 
                       Public Policy Analyst, Non-Profit Organization Director.
                    </span>
                </li>
            </ul>
            <span class="mb-2 block">
                It is conclusive to remark that the department is committed to ensurung that students graduated from  the department do not 
                frustrate the inherent potentcy planted in them by the end of their training in the department. As such students have over 
                the years been linked to scholoarships in the various universities to enable them aspire and secure their dream carrers in science.
            </span>
        </div>
    </div>
</x-page-content>