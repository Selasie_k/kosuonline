<x-page-content>
    <x-slot name="title">
        The Social Sciences Department
    </x-slot>
    <div>
        <h1 class="font-bold mb-6">PROFILE OF  HEAD OF DEPARTMENT</h1>
        <div class="text-justify mb-6">
            <div class="mb-6">
                <img src="/images/hods/social.png" class="h-32 sm:h-48 rounded-lg mb-1 mr-4 float-left" alt="hod of social sc dept">
                <b>Sulley Braimah Mohammed </b> is a native of Asato, born on the 4th of March, 1966. He had his basic Education at 
                Asato - EP Middle School and completed in the year, 1982. He futhured to Kadjebi-Asato Secondary (1982-1987) , then to 
                Jasikan college of Education from 1989 to 1991. He schooled at University of Ghana from 2002 to 2005, and then obtained a 
                post Graduate Diploma In Educadtion (PGDE) from Valley View University in 2018.
                <br>
                As an employee of the Ghana Education Service, and having served at the basic level for some time, he had his maiden feel 
                of second cycle tuition as a class teacher at Kadjebi-Asato Senior High School, in October , 2005. He has since been serving 
                as an instructor in the subject called government and has recently been appointed as the Senior House father of the School. 
                Hitherto, he had served at various positions in the school ranging from class advisor, KASEC P.T.A. Secretary, School Estate 
                Officer, Acting Senior House Father and head of Social Science Department.
            </span>
            <span class="mb-2 block">
                As the head of Social Science Department he oversees the operations of six sub-departments, namely, the Languages, Economics, 
                Geography, Goverment, CRS and History.
            </span>
            <span class="mb-2 block">
                He is a dynamic tutor in his area of tution, has shown a lot of professionalism in his delivery in the classroom, which has 
                endeared him to students in his department.
            </span>
            <span class="mb-2 block">
                He  is a senior fellow that has a lot of say in the daily activities of students on the campus. His activities tend to positively 
                influence junior staff members whom he mentors with a lot of inspiration as well.
            </span>
        </div>

        <h1 class="font-bold">CAREER PROSPECTS FOR SOCIAL SCIENCE STUDENTS </h1>
        <div class="text-justify mb-12">
            <ul class="ml-6 my-4">
                <li class="list-disc">Career Diplomats</li>
                <li class="list-disc">Language interpreters</li>
                <li class="list-disc">Political scientist</li>
                <li class="list-disc">Lawyers</li>
                <li class="list-disc">Economists</li>
                <li class="list-disc">Lecturers and Secondary School teachers</li>
                <li class="list-disc">Research fellows in the field of humanities</li>
                <li class="list-disc">Meteorologist</li>
                <li class="list-disc">Pilots</li>
                <li class="list-disc">Nurses</li>
                <li class="list-disc">Security Services and many more</li>
            </ul>
        </div>
    </div>
</x-app-layout>