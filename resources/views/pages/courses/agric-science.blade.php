<x-page-content>
    <x-slot name="title">
        The Agric Department
    </x-slot>
    <div>
        <h1 class="font-bold mb-6">PROFILE OF  HEAD OF DEPARTMENT</h1>
        <div class="flex text-justify">
            <div class="mb-6">
                <img src="/images/hods/agric.png" class="h-32 sm:h-48 rounded-lg mb-1 mr-4 float-left" alt="hod of social sc dept">
                My name is <b>Nyamesekpor Michael Ofoe. </b> I am 40 years of age. I completed Akuse Methodist Sec tech School in the year 1998, 
                Asuansi farm Institute in the year, 1999, Jasikan College of Education in the year 2004 and University of Education, Winneba in 
                the year 2011. <br> In all the institutions stated above, I pursued Agricultural Science. I was posted to KASEC in the year 2011 and 
                became the head of the Agric Department after serving as a class teacher for five years and have since served in that capacity.
                <br>
                My quest for excellence and display of professionalism in my career endears me to the KASEC community.
            </div>
        </div>

        <h1 class="font-bold mb-4">PROSPECTS WITH AGRICULTURE</h1>
        <div class="text-justify mb-12">
            <span class="mb-2 block">
                Agriculture – Being the core of every nation’s development, especially in this 21st century has seen massive investment in recent times. 
                A situation which is very common in both developed and developing countries including Ghana. Recent developments in the sector has eroded 
                earlier impressions about the name Agriculture. No longer does entering the Agriculture industry mean becoming a farmer.
            </span>
            <span class="mb-2 block">
                It is common occurrence now to chance upon large scale farms unlike previously. On this farms sophisticated agric machinery is always employed 
                by owners - generating large amount of revenue and job opportunities. It is no doubt that Agriculture is an economy booster.
            </span>
            <span class="mb-2 block">
                Some of the Job prospects offered by the Agricultural sector have metamorphosed from the seemingly old, mechanical and unpleasant challenges to 
                recent sophisticated and modernised ones. It is noteworthy that some of these transformed forms of agric sector jobs did not exists a decade 
                ago- making agriculture the “darling boy” in recent times. These carrier paths includes
            </span>
            <ul class="ml-6 my-4">
                <li class="list-disc">Food scientist</li>
                <li class="list-disc">Veterinarian</li>
                <li class="list-disc">Farm workers and labourers</li>
                <li class="list-disc">Horticulturists</li>
                <li class="list-disc">Agriculture Equipment operators</li>
                <li class="list-disc">Agric Economists.</li>
                <li class="list-disc">Agronomists.</li>
                <li class="list-disc">Book keeping, Accounting and Audit clerks</li>
                <li class="list-disc">Truck drivers</li>
                <li class="list-disc">Food Packagers</li>
                <li class="list-disc">Agriculture and Natural Resources Communicationist</li>
                <li class="list-disc">Agricultural engineers</li>
                <li class="list-disc">Agric teachers</li>
                <li class="list-disc">Farm Managers</li>
                <li class="list-disc">Conservation planners</li>
                <li class="list-disc">Researchers/ Breeders</li>
                <li class="list-disc">Agriculture Extension Officers</li>
            </ul>
        </div>
    </div>
</x-app-layout>