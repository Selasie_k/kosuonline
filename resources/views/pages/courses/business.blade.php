<x-page-content>
    <x-slot name="title">
        The Business Department
    </x-slot>
    <div>
        <h1 class="font-bold mb-6">PROFILE OF  HEAD OF DEPARTMENT</h1>
        <div class="text-justify mb-6">
            <div class="mb-2">
                <img src="/images/hods/business.png" class="h-48 rounded-lg mb-4 mr-4 float-left" alt="hod of business dept">
                <b>CA Rockson Mintah</b> is a passionate and human capital-driven individual who is dedicated to providing career guidance 
                and godly mentoring in bringing up generational thinkers of good ethics, professionalism and integrity.
                He is a teacher and Head, Business Department, Kadjebi-Asato Senior High School. He holds an MPhil in Accounting 
                from the University of Ghana Business School and a B.Ed. (Accounting option) from the University of Cape Coast. 
            </div>
            <span class="mb-2 block">
                He is a Chartered Accountant and certified member of The Institute of Chartered Accountants, Ghana (ICAG). 
                He had his Secondary Education at Kadjebi-Asato Secondary School, one of the finest and enviable Senior High Schools in Ghana. 
            </span>
            <span class="mb-2 block">
                He served as a Teaching Assistant at the Department of Business and Social Sciences Education, University of Cape Coast during 
                the 2011/2012 academic year. Prior to he becoming the Head of Business Department in February, 2017, he had served as a Form Master 
                (2013-2015) and an Assistant House Master from 2015 respectively and doubled in the later till 2019. 
            </span>
            <span class="mb-2 block">
                He has served and continue to serve on various Standing and Ad-hoc committees in the School. He teaches Financial Accounting and 
                Business Management across all Forms and authored a Financial Accounting Text Book (Mastering Accounting for Senior High Schools 
                Volume One, Two and Three). 
            </span>
            <span class="mb-2 block">
                He has research interest in Business Education, Taxation, Public Sector Accounting, Auditing and Management Accounting. Above all, 
                he has vast experience in internal and external auditing in both private and public sector as well as financial management as a 
                freelance consultant.
            </span>
        </div>

        <h1 class="font-bold mb-6">DEPARTMENT OVERVIEW</h1>
        <div class="text-justify mb-12">
            <span class="mb-2 block">
                The Business Department in Kadjebi-Asato Senior High School (KASHS) is one of the leading Foundational Business Education providers in Ghana, 
                and a forefront Academic pillar of the School over the past sixty years. The department provides a purposeful and dynamic career education 
                to bring up future global citizenry who are zealous of tasks to self and the larger society. 
            </span>
            <span class="mb-2 block">
                Given the universality of the department and high demand for our students at levels of the career ladder, our subjects and teaching processes 
                are tailored to ensuring that our students qualify to tertiary institutions to pursue career-oriented programmes in Business, Law, Health 
                Management, Actuarial and Computer Sciences, Economics, Political Science, Nursing, Education and Psychology; and hence to ensure that students 
                develop to become professionals with requisite ethics and integrity to manage sensitively and holistically in different organisations in 
                increasingly and fast growing global environment. 
            </span>
            <span class="mb-2 block">
                We pride ourselves in the knowledge that our students upon completion excel in the WASSCE and continue to the tertiary level to become illustrious 
                in all spheres of business, industrial, community and professional life. The Department sets high academic standards for its students and create 
                inclusive engaging curricula, learning environment and practical exposure through excursions and mentorship programmes in response to the diversity 
                of our students. 
            </span>
            <span class="mb-2 block">
                Science, therefore as a noble programme, offers job opportunites  to trainees in  various carreier paths-some of 
                which have direct associaations with science whiles others  are indirects offshoots of the science programmes. 
                To mention but a few, students pusuing sicence programme end up engageing  in carrers such as 
            </span>
            <span class="mb-2 block">
                Our youthful, energetic and highly motivated academic staff provide students with extra-ordinary and highly acclaimed teaching experiences and other 
                support services relevant to their successful stay on campus. I therefore urge all those who want to pursue Business Education and careers such as Accountants, 
                Auditors, Lawyers, Managers of Health Services and Administrators, Actuarists, Financial Analysts, Insurance Brokers, Tax Professionals, Economists; 
                Educationists, Political Scientists, Social Workers among others to do so from the Business Department in Kadjebi-Asato Senior High School that combines 
                excellence with wisdom and courage.
            </span>
        </div>
    </div>
</x-app-layout>