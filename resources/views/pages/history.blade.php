<x-page-content>
    <x-slot name="title">
        BRIEF HISTORY OF KASEC
    </x-slot>

    <div class="md:flex md:space-x-4">
        {{-- <img src="/images/management/headmaster.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="headmanster"> --}}
        <div>
            {{-- <h1 class="font-bold mb-2">HEADMASTER - REVEREND FATHER DANIEL KWADWO LENWAH, (SVD) </h1> --}}
            <span class="mb-2 block">
                The Convention People’s Party led by Dr Kwame Nkrumah established Kadjebi-Asato Secondary School in 1959. The CPP Government approached the late Nana Osei Bonsu III, the Paramount Chief of Asato Traditional Area and the late Nana Akompi Finam II, the Paramount Chief of Kadjebi Traditional Area who without hesitation gave out the land on which the school is established. The first CPP District Commissioner for the then Buem-Krachi District, Mr. Johnson Robert Dabo fought tooth and nail to have the school established between Kadjebi and Asato in the face of a stiff competition over where to establish the school. Mr. Dabo became the first chairman of the Board of Governors for KASEC.
                <br><br>
                Kadjebi-Asato Secondary School was established under the Ghana Education Trust and funded by Ghana Cocoa Board. The school’s foundation stone was laid by the then Volta Regional Commissioner, Hon. F.D. K Goka on 5th September, 1959 but classes began on 19th January, 1960 with 66 boys with 5 teaching staff. The very first student who entered KASEC among the pioneers was the late Alhaji Innusa Isaka. 
                <br><br>
                In June, 1964, the school presented her pioneer students (56 students) for the Joint School Certificate and GCE Examinations.
                <br><br>
                In the 1975/1976 academic year, the Sixth Form Course (A-Level) was introduced under the Headship of Mr. Henry Kosi Owusu  
                <br><br>
                The motto of Kasec is Consilio et Animis (Latin), translated into English Language as Wisdom and Courage.
                <br><br>
                The vision of the school is to create and promote an enabling environment that will sustain effective teaching and learning and also initiate new directions in academic development of the school for effective implementation of the school’s strategic programmes.
                <br><br>
                The mission statement is to develop and implement a high class learning and effective knowledge dissemination, ensuring that children of school age who gain admission to the school are given sound formal quality education, using efficient and effective resource management practices. 
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-6"></div>
    <div class="md:flex md:space-x-4">
        <div>
            <h1 class="font-bold mb-2">THE BOARD OF GOVERNORS OF KASEC COMPRISES</h1>
            <span class="mb-2 block">
                (a) Three members representing the Minister of Education <br>
                (b) One member representing Teaching Staff <br>
                (c) One member representing Traditional Interest <br>
                (d) One member representing Non-teaching Staff <br>
                (e) Three members representing Old Students <br>
                (f) Two members representing Kadjebi District Assembly <br>
                (g) A representative of Kadjebi District Director of Education <br>
                (h) Headmaster of the School <br>
                (i) An Assistant Headmaster <br>
                (j) PTA Representative <br>
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-6"></div>
    <div class="md:flex md:space-x-4">
        <div>
            <h1 class="font-bold mb-2">ROLL OF HEADMASTERS OF KASEC</h1>
            <span class="mb-2 block">
                Mr A.D Addo Yobo---------------1960 to 1965  <br>
                Mr S. K Afari-----------------------1965 to 1972 <br>
                Mr. J.B Yegbe----------------------1972 (Acting Capacity)  <br>
                Mr H.K Owusu---------------------1973 to 1981 <br>
                Mr. E.K Gawu----------------------1981-1984 (Acting Capacity)  <br>
                Mr E.K Kokoroko------------------1984 to 1986 <br>
                Mr. I.K Cudjoe---------------------1986 to 1996  <br>
                Mr F.K Korwu----------------------1996 to 2003	 <br>
                Mr Mussah Yamba Issahaku------2003 to 2009 <br>
                Mr Thomas Fourdjour-Ababio----2009 to 2017 <br>
                Mr Gideon Tay----------------------2017 to 2020  <br>
                Rev. Father Daniel Kwadwo Lenwah, (SVD)----------------------2020 to date  <br>
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-6"></div>
    <div class="md:flex md:space-x-4">
        <div>
            <h1 class="font-bold mb-2">HOUSES IN THE SCHOOL </h1>
            <span class="mb-2 block">
                The students are put into six Houses namely Addo Yobo, Akompi, Volta, Akuafo, Bonsu and Konsu.
                <br><br>
                <strong>Akompi House</strong>  was named after the late Nana Akompi Finam I, Omanhene of Kadjebi-Akan Traditional Area to recognize and appreciate the pioneering role he played in bringing the school to this location 
                <br><br>
                <strong>Konsu House.</strong> Konsu is a stream between the school and Asato which empties itself into the Dai River and serves as a source of water to the school
                <br><br>
                <strong>Addo-Yobo House</strong>  was named in recognition of the dedicated service the first headmaster, Mr. Andrew Danquah Addo-Yobo rendered KASEC
                <br><br>
                <strong>Bonsu House</strong>  was named after the late Nana Osei Bonsu III of Asato Traditional Area to recognize and appreciate the pioneering role he played in bringing the school to this location  
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-6"></div>
    <div class="md:flex md:space-x-4">
        <div>
            <h1 class="font-bold mb-2">SPORTS ACHIEVEMENTS</h1>
            <span class="mb-2 block">
                KASEC won the National Netball Championship from 1962 to 1966 (5 consecutive times) under the able coaching hands of Mr. Stephen Komla Hlodze and taking the Osagyefo Trophy for keeps. Members of the 1964 winning team are: <br>
                Osafo F, Gyanewa D, Dumelo C, Diaba B, Tipong R (Captain), Mensah C and Mesre M. <br>
                Grace Ofori from KASEC was selected out of all athletes in Ghana to represent Ghana at the 2008 Olympic Youth Camp in China. She emerged from the Great KASEC. <br>
                Gabriella Atidokpo was selected as the overall best Sprite Basketball player from KASEC to represent Ghana in Greece in 2009.  <br>
                Upper North Zone which now is the Oti region annually organizes games and athletics for all the 21 Senior High Schools in the Zone or Region. Below are the illustrations of the event from 2014 to 2019 and the overall positions of KASEC <br><br>
                2014=Second position <br>
                2015=First position <br>
                2016=Third position <br>
                2017=Second position <br>
                2018=Third position <br>
                2019= First position
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-6"></div>
    <div class="md:flex md:space-x-4">
        <div>
            <h1 class="font-bold mb-2">AGRICULTURAL ACHIEVEMENTS</h1>
            <span class="mb-2 block">
                As far back as 1963 Mr. Bernard Hanson Ofori a.k.a. Ben Gray who doubled as both the Agriculture and fine Arts Master, identified the agricultural potentials of KASEC. He and the pioneer students began a farm project which remains a permanent feature of the school. <br><br>
                The agricultural potential was confirmed when KASEC won her first agriculture Prize called First Volta Region Agricultural and Industrial Show. The prize was a Tractor <br><br>
                Kadjebi-Asato Secondary School won the National Best School in Agriculture in 1977 and 1990 <br><br>
                Again, the Volta Region Cocoa Quiz Competition held in 2007 was won by KASEC.
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-6"></div>
    <div class="md:flex md:space-x-4">
        <div>
            <h1 class="font-bold mb-2">INFORMATION ON SOME PRINCIPAL OFFICERS</h1>
            <span class="mb-2 block">
                <i>Mr Samuel .D Adinkrah</i> joined the teaching staff in September 1978 as the youngest member to teach English Language and served KASEC till his retirement in in August 2010. Indeed, he was the longest serving member of staff (served for 32 years). <br><br>
                <i>Mr. Gideon Tay</i> was admitted in September, 1973 for O’ Level. He continued and successfully pursued A’ Level from June, 1978 to May, 1980. He became the first Old Student to head the school. 

            </span>
        </div>
    </div>

    {{-- <div class="border-b border-gray-300 my-12"></div>

    <div class="md:flex md:space-x-4">
        <img src="/images/management/head-welfare.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="hod of business dept">
        <div>
            <h1 class="font-bold mb-2">JOY KWABLA MENSAH-ASSISTANT HEADMASTER (WELFARE)</h1>
            <span class="mb-2 block">
                Joy was appointed assistant headmaster of this school in September, 2018. He was teaching at Anlo Senior High School 
                before joining the staff at KASEC. as an assistant headmaster, domestic. He sees to the general discipline of the students.
            </span>
            <span class="mb-2 block">
                Joy is married with tthree children; two boys and a girl. He is an assistant director 1
            </span>
        </div>
    </div> --}}

    {{-- <div class="border-b border-gray-300 my-12"></div>

    <div class="md:flex md:space-x-4">
        <img src="/images/management/burser.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="hod of business dept">
        <div>
            <h1 class="font-bold mb-2">SYLVESTER SENYO BORBBY - BURSER</h1>
            <span class="mb-2 block">
                Senyo is currently the school buirsar. he took over from MrR. Emmanuel Baku, who retired in April, 2020.
            </span>
            <span class="mb-2 block">
                He was transferred from ST. Mary’s senior High and Minor Seminary School, Lolobi. He attended the Ho 
                Technical University and the University of Cape-Coast.          
            </span>
            <span class="mb-2 block">
                He is a christian and worships with the Church of Pentecost. He is married with six children. Three boys, 
                three girls.
            </span>
        </div>
    </div> --}}
</x-page-content>