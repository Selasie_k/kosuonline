<x-page-content>
    <x-slot name="title">
        60th anniversary speech
    </x-slot>
    <div>
        <p class="font-bold text-xl mb-5">SPEECH DELIVERED BY DR. GIDEON TAY, THE HEADMASTER OF KADJEBI-ASATO SECONDARY SCHOOL ON THE OCCASION OF THE 60TH ANNIVERSARY CELEBRATION OF THE SCHOOL ON SATURDAY 16TH NOVEMBER 2019 AT THE FORECOURT OF THE SCHOOL’S ASSEMBLY HALL.</p>

        <span class="mb-2 block">
            <b>
                Mr. Chairman, Your Excellency, the Vice President of the Republic of Ghana, Honourable Regional Minister, 
                the District Chief Executive of Kadjebi District Assembly, Nananom of Kadjebi and Asato Traditional Areas, 
                Chiefs and Elders from the Muslim Community, Heads of Security 
                Agencies Present, Heads of Departments and Agencies here present, Heads of Sister Schools, Ladies and Gentlemen, 
            </b>
                I welcome you all to the 60th anniversary celebration of our alma mater, Kadjebi-Asato Secondary School. As an old 
                student at the helm of affairs now, I must say it is a nostalgic moment for me as I recall the good old days. May 
                the departed souls of students, former heads, teachers and other stakeholders rest with the Lord as we those living 
                continue to work hard to lift high the name and image of our alma mater. 
            
        </span>

        <p class="font-bold my-3">HISTORY OF THE SCHOOL</p>

        <span class="mb-2 block">
            Kadjebi-Asato Secondary School was established in 1959 as one of the Ghana Education Trust Schools. Mr. F.D.K Goka, 
            the Volta Regional Commissioner for Education at the time, laid the foundation stone. Sixty-six boys started the school 
            and five teaching staff in 1961 anchored on the Motto: <b> Consilio et Animis </b> to wit, <b> Wisdom and Courage.</b> Mr. Addo-Yobo of 
            blessed memory served as the first headmaster of the school. The school has a total land size of 659 acres which was 
            released by the chiefs and elders of Kadjebi and Asato traditional areas, hence the name KADJEBI-ASATO Secondary School. 
            Courses offered by the school offers include, Visual Arts, Business, Agricultural Science, General Arts, Home Economics and 
            General Science. The school is considering introducing Technical Programmes for study in some few years to come. Aside from 
            the above, Mr. Chairman, the school has earned enviable results and records in the fields of academic, sports, agriculture etc. 
            Mr. Chairman, we have been riding on the back of some of these achievements, some were added and we hope that more will be 
            added in the near future. 
        </span>

        <p class="font-bold my-3">POPULATION-STAFF & STUDENTS-MALE/FEMALE</p>

        <span class="mb-2 block">
            The current population of the school stands at two thousand and thirteen (2,013) students, with a male population of 963, and 
            female population of 1,050. The school also has 111 teaching staff and 65 non-teaching staff. 
        </span>

        <p class="font-bold my-3">ACHIEVEMENTS OF THE SCHOOL</p>

        <span class="mb-2 block">
            Mr. Chairman, Your Excellency the Vice President, Nananom, Distinguished Guests, Ladies and Gentlemen, as a school we are not only 
            interested in the academic development of our students, but also committed to developing the totality of our students to enable 
            them be functional citizens of Ghana. We have therefore put in place other programmes aimed at unearthing the innate talents of our 
            students especially in the fields of sports, the arts and entertainment. We also have put in place programmes that inculcate high 
            moral standards in our students.
        </span>

        <p class="font-bold my-3">ACADEMIC PERFORMANCE</p>

        <span class="mb-2 block">
            As an academic institution that prepares students for tertiary education, we have always based our performance at SSCE/WASSCE on 
            grades that qualify our students to gain admission into higher institutions of learning in the country and beyond our borders. 
        </span>

        <p class="font-bold my-3">SPORTS</p>

        <span class="mb-2 block">
            Ladies and Gentlemen, the school emerged champions of the National schools and Colleges Netball Championship from 1962 to 1966 
            (five consecutive years). The result was that the Osagyefo Trophy became a permanent property of KASEC.
            In 2007, Grace Ofori of KASEC was voted the Best Schools and Colleges Female Athlete in Ghana and represented Ghana at the 2008 
            Olympic Youth Camp in China. Also, Gabriella Atidokpo was voted the best Female Sprite Basketball player, at the sprite Basketball 
            tournament in December 2008 in Accra and represented Ghana at the Olympic Basketball competition in Greece in 2009.
            For three consecutive years (2007 to 2009), the school was the Volta Regional Schools and Colleges Girls’ Volleyball Champions. 
            The school’s Girls Volleyball team also represented the Volta Region in the Milo Games in Sunyani (28th June-4th July) and in Kumasi 
            (4th-11th July) in 2009. The Upper North Zone, which is now in the Oti region, annually organizes games and athletics for all the 
            twenty-one (21) Senior High Schools in the region. Below are the outcome of events from 2014: to 2019 and the overall positions of the school: <br>
            2019= First position <br>
            2018=Third position <br>
            2017=Second position <br>
            2016=Third position <br>
            2015=First position <br>
            2014=Second position <br>

        </span>

        <p class="font-bold my-3">AGRICULTURE </p>

        <span class="mb-2 block">
            Mr. Chairman, Your Excellency, Distinguished Guests, as a school versed in agriculture as a result of the availability of large 
            arable land and good rainfall patterns, the school has made maximum use of the above-mentioned natural endowments to the benefit 
            of the school and the Agricultural Science Department. Mr. Bernard Hanson Ofori, the first Senior House Parent, established the 
            school farm in 1968 with a 7-acre oil palm plantation and a 4-acre citrus orchard. In 2007, the school won the then Volta Region 
            Cocoa Quiz Competition. The agricultural potential was confirmed when KASEC won her first agriculture Prize at the First Volta 
            Region Agricultural and Industrial Show as well as the following: <br><br>
            &bull;	The Best Secondary School in Agriculture National: Award in 1977. <br>
            &bull;	The Best Secondary School in Agriculture Award on National Farmers Day in Wenchi on 7th December, 1990. <br><br>
            Your Excellency, Ladies and Gentlemen, the school currently has a piggery, a three-acre palm plantation, a three-acre orchard, two 
            acres plantain plantation and three acres of mango plantation. The proceeds from these farms are usually used to supplement student’s 
            meals. We have also started house level farming in the school to encourage and inculcate farming into our students. This is to make 
            farming attractive to the teaming youth and not a job for failures.  
        </span>

        <p class="font-bold my-3">ONGOING PROJECTS </p>

        <span class="mb-2 block">
            Since the establishment of the School in 1959, the School saw her biggest infrastructural expansion under the NPP government of His Excellency 
            President J.A. Kuffour; a 1,500 seater Assembly Hall, a 12-unti Classroom block, Home Economics Lab, ICT lab, a 6-unit semi-detached staff 
            bungalow, 5 detached staff bungalows two dormitory blocks (one each for boys and girls) and a School Clinic. Currently, the Ministry of 
            Education, under another NPP Government led by His Excellency Nana Addo Dankwa Akuffo Addo through GETFUND is constructing a 6-unit classroom 
            block and a 2, 2-number single story dormitory blocks (one each for boys and girls). There is also an ongoing construction of a 10-seater 
            institutional toilet with a bio digester, a solar powered water supply system and a sports complex being undertaken by the Coastal Development 
            Authority. All these important projects, when completed will enhance teaching and learning, improve learning outcomes and help in the discovery and 
            development of the sporting talents of our students and members of the School community. <br> <br>
            Distinguished Guests, Ladies and Gentlemen, we are also embarking on serious landscaping and re-shaping of most of our lawns to beautify the campus 
            and create a serene academic environment. You may also have seen a ten-capacity garage for use by staff and visitors to the school. We have also procured 
            a 45kv power generator for use by students and some essential service providers in the school such as the kitchen. This enables students to study when 
            the national grid goes off, and of course we need a larger capacity generating set to cover the whole school in the future during outages.
        </span>

        <p class="font-bold my-3">BASIC SCHOOL </p>

        <span class="mb-2 block">
            Mr. Chairman, Your Excellency the Vice President, Ladies and Gentlemen in 2017, KASEC established its Basic Primary school with seed money from 
            Teaching staff members of the school and some other donors from outside the school community. We say a very big “THANK YOU” to them. We are still 
            appealing for more support as we try hard to put up permanent structures for them. The school is currently being run from some structures of the 
            school and makeshift structures. It is also our highest expectations that the Model School becomes the first public boarding basic facility in the Oti region.  
        </span>

        <p class="font-bold my-3">CHALLENGES</p>

        <span class="mb-2 block">
            Mr. Chairman, Your Excellency the Vice President, distinguished Guests, there are a lot to complain about as an institution for the nurturing of the body, 
            mind and soul as far as challenges are concerned. The first of them I would like to mention is the high level of indiscipline among students in recent 
            years and the fight for human rights. The increasing use of drugs among our students especially, the girls is a major cause of concern. The use of what 
            they call “wee toffee” rather than smoking is on the rise among our students. I must be quick to let you know that we are doing our very best to contain 
            and deal with the situation. We equally call on all parents to as a matter of necessity monitor the behaviour patterns of their wards at home so that they 
            can provide proper guidance and counselling for them. Parents again must be more responsible as government tries to take off the greater burden of paying 
            school fees. Ladies and Gentlemen, imagine a parent who gives only transportation fare to the ward, one “Ghana must go” sack and a dress for school. <br> <br>
            Mr, Chairman, Your Excellency the Vice President of the republic of Ghana, Nananom, Ladies and Gentlemen, another problem seriously affecting the school is 
            the intake the school gets from the Computer School Selection and Placement System. The school gets students from the Junior High Schools with aggregates 
            as low as 46-50, and we, are expected to turn these students into aggregate six materials. Nevertheless, we are doing such miracles here in KASEC. Students 
            have entered KASEC with aggregates as low as 24, but left the school with aggregate nine (9) or better in the very recent years. <br> <br>
            Your Excellency the Vice President, Distinguished Guests, still related to academic work, we would want to appeal for a befitting school library to serve 
            the increasing population. The library we are currently depending on is very small for the population hence students’ inability to fully utilise the facility. 
            Ladies and Gentlemen, every good library requires modern books and I must say that most of the books in our library have information dating to the eighteenth 
            century. Modern books with very recent information for use, would serve a great deal to our students. Mr. Chairman, Ladies and Gentlemen, we, as a school do 
            not have administrative vehicle for daily operations. This is making running daily affairs of the school difficult. We mostly have to depend on personal vehicles 
            of some members of staff for official and administrative duties. Students sometimes would also have to use hired vehicles to travel for academic and co-curricular 
            programmes.  We are therefore appealing for administrative vehicle and a school bus for smooth running of activities in the school. Ladies and Gentlemen, 
            the school’s science resource centre is in such a deplorable state. We appeal for urgent government support to enhance the teaching and learning of the various 
            subjects in all departments. We also appeal to you to help the school’s clinic to be upgraded to a CHPS Compound to see to the health needs of the academic environment.   
  
        </span>

        <p class="font-bold my-3">THE WAY FORWARD </p>

        <span class="mb-2 block">
            Mr. Chairman, Your Excellency the Vice President, we, at this point want to call for continuous support from our partners in education. We must not relent in 
            our quest to raise true future leaders for our nation, Ghana. We call on parents also to do their best at augmenting government’s efforts at providing free 
            access to education for all. <br><br>
            Mr. Chairman, Your Excellency, the Vice President, Ladies and Gentlemen, I would also want to appeal to government to consider supplying elective subject 
            course materials to students, even if at subsidized rates so that students can have access to them for effective academic work. The current situation where 
            students or parents would have to purchase these elective books, most of whom do not bother to get them is not helping our quest to providing quality 
            access to education. 
        </span>

        <p class="font-bold my-3">APPRECIATION AND GRATITUDE </p>

        <span class="mb-2 block">
            We are forever grateful to the government and other stakeholders in education for their tremendous support during the times of need over the past sixty years. 
            We hope to enjoy more collaboration and support in the coming years. We are equally grateful to all former headmasters of the school whose foundation we are 
            building on today. We are indebted to the American and Japanese governments for their assistance in the provision of Mathematics and Science Teachers through 
            the Peace Corps and JOCV respectively. We are also grateful for the Library and the 4-unit semi-detached staff bungalow provided by the American and the Japanese 
            governments respectively. <br><br>
            Finally, Mr. Chairman, Ladies and Gentlemen, may I add my voice to the numerous calls for government to consider as much as possible, the full restoration of 
            Parent-Teacher Associations (PTA) Dues and Levies to assist government provide other important facilities in the school. <br> <br>
            Mr. Chairman, Your Excellency, the Vice President of the Republic of Ghana, Regional Minister, the District Chief Executive of Kadjebi District Assembly, 
            Nananom of Kadjebi and Asato Traditional Areas, Chiefs and Elders from the Muslim Community, Heads of Security Agencies Present, Heads of Departments and Agencies 
            here present, Heads of Sister Schools, we welcome you all to our 60th ANNIVERSARY CELEBRATION. <br><br>
            Thank You. 

        </span>
    </div>
</x-page-content>