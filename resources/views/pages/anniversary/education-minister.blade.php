<x-page-content>
    <x-slot name="title">
        60th anniversary speech
    </x-slot>
    <div>
        <p class="font-bold text-xl mb-5">
            SPECIAL ANNIVERSARY MESSAGE FROM THE DEPUTY MINISTER, GENERAL EDUCATION AND MP FOR BOSOMTWE HON. DR. YAW OSEI ADUTWUM ON THE OCASSION OF THE 60TH ANNIVERSARY CELEBRATION OF KADJEBI ASATO SENIOR HIGH SCHOOL ON THE 16TH NOVEMBER, 2019 AT KADJAEBI 
        </p>

        <span class="mb-2 block">
            <img src="/images/anniversary/education-minister.jpg" class="h-48 rounded-lg mb-4 mr-4 float-left" alt="hod of business dept">
            Education aids man to move from the unknown to the known. In the known, one becomes conscious of themselves and the environment they live in. That is what Kasec has done for 60 years since its inception.
        </span>
        <div class="mb-2 block">
            I congratulate the school for this feat and celebrate the forbearers for their vision in setting up this school to train minds and inspire generations. I also celebrate the latter-day saints that have also managed the institution to this level. 
        </div>
        <span class="mb-2 block">
            It is my hope that the torch of success will be passed to generations un-end. I also hope that the school will continually produce the crop of workforce that Ghana needs in its development agenda. 
        </span>
        <span class="mb-2 block">
            As government instituting different reforms to enhance education in Ghana, I advise parents to enrol their wards in the different levels of education in order to enjoy the new order. 
        </span>
        <span class="mb-2 block">
            I congratulate the leadership and students of KASEC on their anniversary; and I wish them well. 
        </span>
        <span class="mb-2 block">
            God bless our homeland Ghana! <br><br>
            God bless KASEC
        </span>
    </div>
</x-page-content>