<x-page-content>
    <x-slot name="title">
        60th anniversary speech
    </x-slot>
    <div>
        <p class="font-bold text-xl mb-5">
            ADDRESS DELIVERED ON BEHALF OF THE VICE PRESIDENT OF THE REPUBLIC OF GHANA BY  
            HON. BONIFACE ABUBAKAR SADDIQUE DURING THE 60TH ANNIVERSARY CELEBRATIONS OF THE KADJEBI ASATO SECONDARY 
            SCHOOL HELD ON 16TH NOVEMBER 2019 AT KADJEBI-ASATO SECONDARY SCHOOL
        </p>

        <span class="mb-2 block">
            <img src="/images/anniversary/veep-rep.jpg" class="h-48 rounded-lg mb-4 mr-4 float-left" alt="hod of business dept">
            <b> Nana Chairman and Chairman of the Board of Directors of Kasec, Nana Sekyere Bediatuo IV, the Adontenhene of Kadjebi-Akan
            Nananom of Kadjebi and Asato Traditional Areas
            Hon. Minister for Education, Dr. Matthew Opoku Prempeh,
            The Chief Executive Officer of the Coastal Development Authority, Lawyer Jerry Shaib
            Director General of the Ghana Education Service,
            Regional Director of Education,
            District Director of Education,
            Members of the School’s Governing Board
            Headmaster of Kadjebi-Asato SHS,
            Teaching and Non-Teaching Staff
            Executives of the Old Students Union,
            Executives of Students Representative Council,
            Distinguished Invited Guest,
            Members of the Media,
            Students,
            Ladies and Gentlemen: </b>
        </span>

        <div class="mb-2 block">
            It is with the greatest privilege and honour for me to be part of this 60th Anniversary celebration of 
            the Kadjebi-Asato Secondary School (KASEC). <br>
            The Vice President, His Excellency Dr. Mahamudu Bawumia would have been here to celebrate this very 
            important milestone of KASEC with you but other equally important national assignments made it impossible 
            for him to honour your invitation no matter how hard he tried. <br>
        </div>
        <span class="mb-2 block">
            <b>Nana Chairman, Ladies and Gentlemen; </b>
            Before I continue, I will like to congratulate the Headmaster, teaching and non- teaching Staff, the governing 
            board, Old and current students of KASEC people of both Kadjebi and Asato for their support in helping KASEC to her 60th Anniversary.
            I will also like to remember those who had the vision and foresight to create this institution of higher learning 
            that has attained and continues to atain all there is in areas of sports, academia and all human endeavours. 
            To them, I say your vision of yesterday has yielded great dividends not just for Kadjebi and Asato but Ghana as a whole, Thank you.
            To the organizers of this great event, we can only imagine the time, efforts and sleepless nights you put in to 
            give us such a successful event. I say may God replenish all your efforts in several folds.  
        </span>
        <span class="mb-2 block">
            <b>Nana Chairman, Distinguished Guests, Ladies and Gentlemen; </b>
            Kadjebi – Asato Secondary School has produced a lot of human resources to the development of this Country. We find them in all 
            levels of National undertakings. Their contributions to this country cannot be overemphasized. 
            As such, I will entreat all those concerned in the development of KASEC not to rest on their oars, the real job has just 
            began considering the introduction of the Free Senior High School programme introduced by His Excellency Nana Akufo-Addo’s 
            government. This policy implies that there will be more student intake, more people to teach and therefore more work and 
            efforts to put in to achieve the type of results that KASEC is noted for.  
        </span>
        <span class="mb-2 block">
            <b>Nana Chairman, Ladies and Gentlemen;</b>
            It is a well known fact that, the NPP government has done more to KASEC than any government in her 60 years of existence. 
            Since the establishment of the School in 1959, the biggest infrastructural expansion was experienced under the NPP government 
            of former President J.A. Kufour. To mention a few was a 1500 seater Assembly hall, 12-Unit Classroom Block, Home Economics 
            Laboratory, ICT Laboratory, 2 – Dormitory Blocks (one each for boys and girls and a School Clinic. <br>
            Under His Excellency Nana Akufo-Addo’s government, another NPP government, i can confidently say KASEC is experiencing another 
            infrastructure boom. Currently, the GETFUND is constructing a 6-Unit Classroom block, a 2- single storey dormitory blocks 
            (one each for boys and girls), there is also an ongoing construction of a 10-seater institutional toilet with a bio digester, 
            a solar powered water supply system and a Ultra Modern Sports Complex with washrooms and dressing rooms being financed by the 
            Coastal Development Authority under the one million dollar per constituency (IPED)
        </span>
        <span class="mb-2 block">
            <b>Nana Chairman, Ladies and Gentlemen;</b>
            All these projects, when completed, will not just enhance teaching and learning experience in KASEC but will go a long way to
             create more rooms for more students to be admitted considering the exponential increments we are experiencing with the 
             introduction of the Free Senior High School Policy. <br>
            Nana Chairman, be assured of the governments readiness to support KASEC in whatever way possible to ensure KASEC remains a 
            citadel of learning in Ghana.
        </span>
        <span class="mb-2 block">
            <b>Distinguished Invited Guests;</b>
            Managing School these days is no longer the duty of the government and teachers alone. There are other important stakeholders 
            whose contributions are now surpassing that of government in certain schools. <br>
            For KASEC to grow, excel and keep producing high quality students there is the need for all hands to be on deck. The first point 
            of call is the Old Students. Old students have enormous experience of first knowing the needs of the school whilst they were 
            once students, know their individual and collective areas of competences and skills and finally know how to impact their 
            knowledge to the younger ones without much difficulty. <br>
            Another important stakeholder is Nananom. Nananom are the custodians of wisdom. They are the owners of the land and other resources. 
            Nananom, please visit and also invite the teachers and students to the palace and ask questions, counsel them and also provide the 
            School with lands for expansion when needed. <br>
            Lastly, the community of Kadjebi and Asato are also key stakeholders. When KASEC expands and attracts the best of students and 
            teachers, the benefits will reach you. As such, I will plead with the communities of Kadjebi and Asato to take a particular interest 
            in the affairs of KASEC. <br>
            I strongly believe, if we all put our hands on deck, no matter how little or small, KASEC will rise beyond the clouds. I assure you 
            of government support all the way. 
        </span>
        <span class="mb-2 block">
            <b>Nana Chairman, Ladies and Gentlemen;</b>
            On this special day, there is the need to do proper introspection. What did we not do right? What could we have done better? Let us 
            use this day to do a proper forecast of what we want our Great KASEC to be in the next 50 or more years to come. <br>
            For this to be possible, the current and future students must be our focus. They are the future. They are the next generation and 
            country builders. To you, the young ones in school today, I say take advantage of the Free SHS policy, your seniors and Old Students 
            of KASEC to prepare your future from today. You will never regret it if you do.
        </span>
        <span class="mb-2 block">
            <b>
                Nana Chairman and Chairman of the Board of Directors of Kasec, Nana Sekyere Bediatuo IV, the Adontenhene of Kadjebi-Akan, Nananom of Kadjebi and Asato Traditional Areas
                Hon. Minister for Education, Distinguished Invited Guests, Ladies and Gentlemen, 
            </b> <br>
            At this juncture, it’s my pleasure to say HAPPY 60TH ANNIVERSARY to KASEC and all KASECANS here present and all over the world.
            Thank you and may God bless us all. <br><br>
            Long Live KASEC, <br><br>
            Long Live Ghana. <br><br>
        </span>
    </div>
</x-page-content>