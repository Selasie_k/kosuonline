<x-page-content>
    <x-slot name="title">
        60th anniversary speech
    </x-slot>
    <div>
        <p class="font-bold text-xl mb-5">
            SPEECH DELIVERED BY MR. MICHAEL KWASI OTENG, PRESIDENT OF KASEC OLD STUDENTS UNION AT THE KASEC @ 60 ANNIVERSARY CELEBRATION AT THE KADJEBI-ASATO SECONDARY SCHOOL COMPOUND ON THE 16TH OF NOVEMBER 2019.
        </p>

        <span class="mb-2 block">
            <img src="/images/anniversary/kosu-president.jpg" class="h-48 rounded-lg mb-4 mr-4 float-left" alt="hod of business dept">
            <b> Nana Chairman and Chairman of the Board of Directors of Kasec, Nana Sekyere Bediatuo IV, the Adontenhene of Kadjebi-Akan, His Excellency the THE VICE PRESIDENT OF THE REPUBLIC OF GHANA, DR. MAHAMUDU BAWUMIA, the Regent of Kadjebi Traditional Council, the Regent of Asato Traditional Council, the Honourable Minister of Education Dr. Mathew Opoku Prempeh, </b>
            the Honourable Regional Minister of the Oti Region Honourable Kwasi Owusu Yeboah, the Honourable Chief Executive Officer of the Coastal Development Authority (CODA) Lawyer Jerry Shaib, the District Chief Executive of Kadjebi District Assembly, Hon. Maxwell Asiedu, The Regional Director of Education, the District Director of Education, Board Members of KASEC, Former Headmasters of KASEC present, Mrs. Addo Yobo, wife of the first Headmaster of Kasec, represented by her children Dr. Constance Addo-Yobo, Prof. Emmnauel Addo-Yobo and Samuel Addo-Yobo, Headmasters of Sister Schools present, KOSU members here present, Students of KASEC, Our Accredited Media Partners, Invited Guests, Ladies and Gentlemen.
        </span>

        <div class="mb-2 block">
            It is with the greatest honour that we welcome and thank you His Excellency the Vice President DR. MAHAMUDU BAWUMIA, for sacrificing all your busy schedule to be with us today. We are indebtedly grateful.
        </div>

        <div class="mb-2 block">
            We are equally very grateful and appreciative to you the Honourable Minister of Education, Honourable Regional Minister, the Regents of Kadjebi and Asato Traditional Councils for being with us today to make our event an eventful one. 
        </div>

        <h1 class="py-2"><b>BIRTH OF KASEC</b></h1>

        <span class="mb-2 block">
            <b>Nana Chairman, His Excellency,  Distinguished Ladies and Gentlemen, </b>
            some sixty years ago, a tiny seed was sown along the banks of the Konsu River which happens to be the boundary between Kadjebi and Asato all in the then Volta Region now Oti Region of Ghana. That tiny seed, after germination, was named KADJEBI ASATO SECONDARY SCHOOL (KASEC).
        </span>

        <span class="mb-2 block">
            In fulfillment of the declaration in Jeremiah 17:8, KASEC, which was planted by the waters, extended its roots into the stream and never feared drought, always looking green, fresh and yielding good fruits from then till present.
        </span>

        <span class="mb-2 block">
            The fruits of KASEC have dispersed all over the world filling various departments of human undertakings. We have Kasecans as judges, medical doctors, professors, Ministers of state, police officers, army officers and countless other professions. Today is the day for the fruits from this tree to come together to celebrate how motherly KASEC has been to us. It is time to honour, adore, pumper and celebrate her.
        </span>

        <span class="mb-2 block">
            <b>Nana Chairman, His Excellency the Vice President, Hon. Minister Of Education, Hon. Regional Minister, Distinguished Ladies and Gentlemen,  </b>
            Today, the minds that KASEC our Mother has educated and inspired during these sixty years of existence have gathered here to honour her once again. We are here to celebrate this important day of her life to say <b>THANK YOU and HAPPY BIRTHDAY</b> .
        </span>

        <span class="mb-2 block">
            At 60, she is supposed to start her retirement life. However, in the African context, age goes with wisdom. There is an Akan adage which says “when your parents helped you grow your teeth, it’s your duty to help them loose theirs” 
        </span>

        <span class="mb-2 block">
            In order to ensure you have an enjoyable retirement life as well as help you lose your teeth gracefully, the old students have developed some key programs to improve teaching and learning experience in KASEC as well as contribute to the social health needs of the KASEC communities of Kadjebi and Asato. 
        </span>

        <h1 class="py-2"><b>THE ROLE OF KOSU IN TEACHING AND LEARNING</b></h1>

        <span class="mb-2 block">
            <b>Nana Chairman, His Excellency Mr. Vice President, Hounarable Ladies and Gentlemen, </b>
            at the August 2019 KOSU meeting, the Academic Staff of KASEC briefed members on the Academic performance of the School. The troubling news was that academic performance of the school currently is on a nose dive.
        </span>
        <span class="mb-2 block">
            We know the teachers are putting in their best in these challenging times but we believe you can do a little much more. The reward will definitely come and it will be fulfilling. 
        </span>
        <span class="mb-2 block">
            We also deduced from the presentation, Nana Chairman, that the once very DISCIPLINED KASEC has now become the playground for little children. There is serious lack of discipline among both teachers and students.
        </span>
        <span class="mb-2 block">
            We all know, as a fact that, nothing good can come out of such an environment of indiscipline. To forestall this declining trend of the School, KOSU has developed the following interim measures.
        </span>
        <span class="mb-2 block">
            1.  A three member team made up of Professor Anthony Kruegar, Professor Mrs. Docea Fianu and Dr. Lord Cephas Mawuko-Yevugah has been formed with the mandate to visit the school every quarter to organize a workshop for both teachers and students.
        </span>
        <span class="mb-2 block">
            2.  Additionally, KOSU is also introducing a mentoring programme where every quarter, some selected KOSU members will visit the School to interact with both teachers and students through the sharing of their life experiences at school and after school. This is intended to motivate especially the students to study well.
        </span>
        <span class="mb-2 block">
            <b>Nana Chairman, ladies and gentlemen, </b>
            I vividly remember during our days in school when Kasecans from the University of Ghana were visiting. We were made to believe that mosquitoes don’t bite graduates. They will land on your body alright, but will not bite because you are a graduate. Being young and unaware of the deception, we were psychologically energized to study hard to become graduates in order not to be bitten by mosquitoes.  
        </span>
        <span class="mb-2 block">
            3. As a top up, KOSU is introducing an awards scheme to award the best performing student in the core subjects of English, Mathematics and Science. The overall best student with a grade of A1-B1will be given a tablet as an award.
        </span>
        <span class="mb-2 block">
            4. There is also a Prof. Mrs. Docea Fianu Award for the best Consistent Clothing and Textiles Student. This will be in the form of a Sewing Machine with an electric motor.
        </span>
        <span class="mb-2 block">
            5. Kosu will also liaise with the Board to introduce certain level of punishment. Growing up in Kadjebi was an interesting experience. There was total discipline. The upbringing of the child was the responsibility of the Community and not the parents alone. One could be disciplined for misbehaving in town by any member of the community before reporting you to your parents who will in turn punish you for disgracing the family name.
        </span>
        <span class="mb-2 block">
            Where are all those levels of discipline we were used to? Madam Regional and District Directors of Education, please let us revisit the days of old. Things are gradually getting out of hands. “A stitch in time saves nine” We cannot spare the rod and spoil our future generation.   
        </span>

        <h1 class="py-2"><b>KOSU AND THE COMMUNITIES OF KADJEBI AND ASATO</b></h1>

        <span class="mb-2 block">
            <b>Nana Chairman, His Excellency the Vice President, Invited Guests. </b>
            As the saying goes, “a sound mind in a sound body”. The health of the peoples of Kadjebi and Asato is very paramount to KOSU. It is only then that they can build a strong and healthy community.
            In order to ensure that the people of these communities are healthy, KOSU in collaboration with the Coastal Development Authority (CODA) are constructing a Multi Purpose Sports Complex in KASEC. This project is estimated to cost Six Hundred Thousand Ghana Cedis (GHC 600,000.00). The playing area of the Complex consists of a Volleyball, Basketball, Long Tennis, Hand ball and netball. It also has a Changing room with washroom facilities plus a Spectator Stand around the whole court.
        </span>
        <span class="mb-2 block">
            This complex, when completed, will not only help improve the physical health of the students and people, but should also be used to discover new talents from these communities onto the national sports platforms.   
        </span>
        <span class="mb-2 block">
            Nana Chairman, KOSU has successfully organized three days Health Screening Exercise in both Kadjebi and Asato. There was a Free Prostate Screening for men above 40 years with Free Medication for Men above 60 years as well as a Free Dental Screening and Treatment for all who patronized the venues. The total cost of these Screening and treatment exercise was Sixty Thousand Ghana Cedis (GHC 60,000.00)
        </span>
        
        <h1 class="py-2"><b>KOSU AND THE PACESETTERS </b></h1>
        
        <span class="mb-2 block">
            Nana Chairman, Distinguished Ladies and Gentlemen, There is an Akan saying that “sankofa yenkyi” literally meaning if you forgot something and later goes for it is not a crime. KOSU realized that there are certain individuals who sacrifice all they have to place KASEC on the map of high academic learning and sports.
        </span>
        <span class="mb-2 block">
            All over the years, these Heroes of KASEC have never been recognized let alone celebrated. As a result, KOSU made it a point to locate those that are readily available for recognition. Notable among them are Mr. Andrew Danquah Addo-Yobo, who was the pioneer Headmaster of KASEC. 
        </span>
        <span class="mb-2 block">
            Another very important personality in the History of the making of KASEC what it is today is Mr. Johnson Robert Dabo who was the first District Commissioner for Buem-Krachi. He was very instrumental in the establishment of KASEC. 
        </span>
        <span class="mb-2 block">
            We also have Mr. Bernard Hanson Ofori a.k.a. Ben Gray who doubled as both the Agriculture and fine Arts Master. He was the architect of the landscaping of KASEC. He was very instrumental in the planting of the Royal Palms that we see lining the street from the entrance to the school. 
        </span>
        <span class="mb-2 block">
            The last but not the least is Mr. Stephen Komla Hlodze. He was the Sports master who catapulted KASEC to an unimaginable height in sports. He coached the Netball team to win the Osagyefo Netball Trophy for five (5) consecutive years thereby giving KASEC the right to keep the Trophy for good. There were many more of such people but time and space will not allow us. To all these Heroes, we say THANK YOU.
        </span>
        <span class="mb-2 block">
            Nana Chairman, we also have two individuals who have distinguished themselves in KASEC and KOSU related matters. Mr. Ameko Worlako Kofi was the one who designed the anniversary cloth that we all put on today and looking beautiful and handsome. Mr. Senoo Peter Addison, with his camera and video recorder on his shoulders has travelled to interview most of the former Headmasters and personalities who were willing for a yet to be released documentary on KASEC. They have not charged anything and we have not given them anything yet but they still continue to give off their best. On this special day, we say to you brothers, God bless you abundantly.   
        </span>

        <h1 class="py-2"><b>THE OSAGYEFO TROPHY</b></h1>

        <span class="mb-2 block">
            Nana Chairman, Even though KASEC won the Osagyefo Trophy for keeps, I believe most of us seated here today have never seen it before. Nobody here knows where it is. Interestingly, the Trophy left KASEC as far back in the late 1960s and never returned. 
        </span>
        <span class="mb-2 block">
            Through investigations by KOSU, we got to know the Trophy was last seen in the late 1970s at the Kumasi Sports Stadium. KOSU has written to the National Sports Authority requesting the return of the Trophy to KASEC. The process is still ongoing. With the help of a friend of KOSU and KASEC in the person of the CEO of CODA Lawyer Jerry Shaib, we believe strongly that in due time, the Trophy will be located and handed back to KASEC. 
        </span>
        
        <h1 class="py-2"><b>SCHOOL CADET</b></h1>
        
        <span class="mb-2 block">
            Nana Chairman, Distinguished Ladies and Gentlemen, you will all agree with me that, the School Cadet system is becoming another form of opportunity for our students to get admission into the forces. KOSU being mindful of this decided to assist KASEC to also have a well recognized and formidable Cadet Corps. 
        </span>
        <span class="mb-2 block">
            To realize this dream, KOSU with the assistance of ACP David Eklu who is also a KOSU member and a product of KASEC and some officers at the Police Headquarters, KOSU has been able to retool the KASEC Cadet Corps by providing them with a brand new sets of almost everything needed except the uniform. We are determined to ensure that by the end of the coming year, our Cadet Corps will start wearing their own KASEC labeled uniform. ACP David Ekly, KOSU are KASEC are very grateful.
        </span>
        
        <h1 class="py-2"><b>KOSU AND THE FUTURE</b></h1>
        
        <span class="mb-2 block">
            We know that there are some Kasecans out there who vowed not to have anything to do with either KASEC or KOSU. To them, I say better look out and revise your stand. The KOSU train is on the move. The stops are very few. We are not waiting long at any stop. We are developing a very happy family. You either join today or regret not doing so tomorrow.
        </span>
        <span class="mb-2 block">
            The other group of Kasecans sitting on the fence, be careful, the fence is being eaten down by termites. You might not land well when the fence collapses.
        </span>
        <span class="mb-2 block">
            This is the time to propagate the message of revival. The message of transparency, accountability and togetherness. This is the time to stand and be counted. The elders say, it is better to be a slave in your own town than become a king in another person’s town. The doors of KOSU are always opened. All are always welcomed. The venue of our meetings is still Christ the King Hall. Just pay us a visit and you will never regret you did.
        </span>
        
        <h1 class="py-2"><b>CONCLUSION</b></h1>

        <span class="mb-2 block">
            Organizing this event was not an easy task. It required the suspension of all immediate gratifications. It involved a lot of financial and material resources laced with a very high level of commitment. We did our best. There were some who prayed for us, we say God bless you. Others encouraged us. To them, we say THANK YOU. Many others gave us their time and money while a number also called to wish us well. To these people, we will forever be indebtedly grateful. May the Almighty keep keeping you all safe, strong and healthy. Amen.
        </span>

        <b>
            God Bless KASEC,  <br>
            God Bless KOSU, <br>
            God Bless our Homeland Ghana. <br><br>

            KONSU SAKYI – NYANSAPOR!!! <br>
            THANK YOU   <br>

        </b>
    </div>
</x-page-content>