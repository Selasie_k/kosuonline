<x-page-content>
    <x-slot name="title">
         Management of KASEC
    </x-slot>

    <div class="md:flex md:space-x-4">
        <img src="/images/management/headmaster.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="headmanster">
        <div>
            <h1 class="font-bold mb-2">HEADMASTER - REVEREND FATHER DANIEL KWADWO LENWAH, (SVD) </h1>
            <span class="mb-2 block">
                Father Lenwah is a catholic priest, belonging to the Society of the Divine Word (SVD). <br>
                He took over from MR. Gideon Tay as the substantive headmaster on 30th December,2019. 
                Until his appointment as the headmaster of this noble institution, he was the headmaster of 
                ST. Dominic’s Senior High Technical School, Kwahu-Pepease.
            </span>
            <span class="mb-2 block">
                Before his appointment as a substantive head of St. Dominic’s, he worked as an assistant 
                headmaster, administration at St. Peter’s Senior High School for twelve years.
            </span>
            <span class="mb-2 block">
                He has three masters degree from various universities in Japan. he speaks the Japanese 
                language fluently. He is a Deputy director of education.
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-12"></div>

    <div class="md:flex md:space-x-4">
        <img src="/images/management/head-accademics.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="hod of business dept">
        <div>
            <h1 class="font-bold mb-2">CEPHAS KWABLA ADANUVOR - ASSISTANT HEADMASTER (ACADEMIC)</h1>
            <span class="mb-2 block">
                Cephas Adanuvor is currently the assistant headmaster in charge of academics. He is a first class 
                graduate from the University of Ghana, where he studied linguistics and english language. 
                Until his appointment as assistant headmaster at Kasec, he was teaching at Adidome Senior High School. 
                His appointment came in September, 2019.
            </span>
            <span class="mb-2 block">
                He is very principled and very committed to improving academic peeformance of the school. 
                As a result of his hardwork, he is currently combining the work of the assistant headmaster, 
                administration with his work as an assistant headmaster, academic.
            </span>
            <span class="mb-2 block">
                He attends the Evangelical Presbyterian Church, Kadjebi, where he is the music director. 
                He is also the music director of the Northern Presbytery of the Evangelical Presbyterian Church.
                He is married with two boys. He is an Assistant Director 1
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-12"></div>

    <div class="md:flex md:space-x-4">
        <img src="/images/management/head-welfare.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="hod of business dept">
        <div>
            <h1 class="font-bold mb-2">JOY KWABLA MENSAH-ASSISTANT HEADMASTER (WELFARE)</h1>
            <span class="mb-2 block">
                Joy was appointed assistant headmaster of this school in September, 2018. He was teaching at Anlo Senior High School 
                before joining the staff at KASEC. as an assistant headmaster, domestic. He sees to the general discipline of the students.
            </span>
            <span class="mb-2 block">
                Joy is married with tthree children; two boys and a girl. He is an assistant director 1
            </span>
        </div>
    </div>

    <div class="border-b border-gray-300 my-12"></div>

    <div class="md:flex md:space-x-4">
        <img src="/images/management/burser.jpeg" class="border shadow h-32 md:h-48 rounded-lg mb-4" alt="hod of business dept">
        <div>
            <h1 class="font-bold mb-2">SYLVESTER SENYO BORBBY - BURSER</h1>
            <span class="mb-2 block">
                Senyo is currently the school buirsar. he took over from MrR. Emmanuel Baku, who retired in April, 2020.
            </span>
            <span class="mb-2 block">
                He was transferred from ST. Mary’s senior High and Minor Seminary School, Lolobi. He attended the Ho 
                Technical University and the University of Cape-Coast.          
            </span>
            <span class="mb-2 block">
                He is a christian and worships with the Church of Pentecost. He is married with six children. Three boys, 
                three girls.
            </span>
        </div>
    </div>
</x-page-content>