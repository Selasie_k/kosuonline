<x-app-layout>
    <div class="max-w-screen-lg mx-auto py-8 px-4">
        {{-- <div class="md:flex md:space-x-4">
            <img src="{{$exec['image']}}" alt="excecutive" class="mx-auto h-64 md:mx-0 rounded-md bg-white">
            <div class="text-center md:text-left">
                <p class="text-gray-600 pt-3">{{$exec['position']}}</p>
                <p class="text-2xl">{{$exec['name']}}</p>
                <p class="text-gray-700">{!! $exec['profile'] !!}</p>
            </div>
        </div> --}}

        <div class="pb-6">
            <div class="md:flex md:space-x-8">
                <img src="{{$exec['image']}}" alt="excecutive" class="h-48 mb-4 mx-auto rounded-full shadow-lg">
                <div class="text-center md:text-left flex-1">
                    <p class="text-sm text-gray-700">{{$exec['position']}}</p>
                    <p class="font-bold mb-2 pb-2 border-b">{{$exec['name']}}</p>
                    <p class="text-left">{!! $exec['profile'] !!}</p>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>