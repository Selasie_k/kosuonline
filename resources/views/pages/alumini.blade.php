<x-app-layout>
    <div class="max-w-screen-lg mx-auto p-8">
        <div class="sm:flex justify-between">
            <button class="px-4 py-1 bg-red-800 text-white rounded mb-4 w-full sm:w-auto">Donations / Dues</button>
            <div class="flex justify-between">
                <button class="px-4 py-1 bg-red-800 text-white rounded mb-4 mr-4">Login</button>
                <button class="px-4 py-1 bg-red-800 text-white rounded mb-4">Create Profile</button>
            </div>
        </div>

        <div class="my-16">
            <p class="text-2xl font-bold text-red-900 uppercase text-center">Welcome to the Kasec Old Students Union</p>
        </div>

        <div class="flex flex-wrap justify-center">
            @foreach (config('kasec.kosu.executives') as $exec)
                <a href="{{route('executive-profile', ['name' => $exec['name']])}}" class="w-full sm:w-1/2 md:w-1/3 flex flex-col items-center space-y-2 mb-8 hover:text-blue-600 cursor-pointer">
                    <img src="{{$exec['image']}}" alt="excecutive" class="h-48 rounded-lg bg-white shadow hover:shadow-lg">
                    <div class="flex-1 text-center">
                        <p class="text-sm text-gray-700">{{$exec['name']}}</p>
                        <p class="font-bold mb-2">{{$exec['position']}}</p>
                    </div>
                </a>
            @endforeach
        </div>

        {{-- <div class="pb-6">
            <div class="md:flex md:space-x-8">
                <img src="/images/kosu/president.jpg" alt="excecutive" class="h-48 mb-4 rounded-md bg-white">
                <div class="flex-1">
                    <p class="text-sm text-gray-700">Headmaster</p>
                    <p class="font-bold mb-2">Welcome to Kadjebi-Asato Senior High School</p>
                    <p class="text-justify">
                        Kadjebi-Asato Secondary School was established in 1959 as one of the Ghana 
                        Education Trust Schools. Mr. F.D.K Goka, the Volta Regional Commissioner 
                        for Education at the time, laid the foundation stone. Sixty-six boys 
                        started the school and five teaching staff in 1961 anchored on the 
                        Motto: Consilio et Animis to wit, Wisdom and Courage. Mr. Addo-Yobo 
                        of blessed memory served as the first headmaster of the school. 
                        The school has a total land size of 659 acres which was released...
                    </p>
                </div>
            </div>
        </div> --}}
    </div>
</x-app-layout>