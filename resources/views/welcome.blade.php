<x-app-layout>
    <vue-carousel></vue-carousel>

    {{-- Headmaster --}}
    <section class="py-16">
        <div class="container">
            <div class="md:flex">
                <div class="mx-auto text-center sm:mx-8">
                    <img src="/images/management/headmaster.jpeg" alt="headmaster" class="mx-auto h-56 mb-4 rounded-md bg-white">
                    <p class="text-sm text-gray-700">Headmaster</p>
                    <p class="text-sm mb-8">REVEREND FATHER DANIEL <br> KWADWO LENWAH, (SVD)</p>
                </div>
                <div class="flex-1">
                    <p class="font-bold text-red-900 tracking-widest uppercase sm:text-2xl mb-4">Welcome to Kadjebi-Asato Senior High School</p>
                    <p class="mb-6 text-justify">
                        Kadjebi-Asato Secondary School was established in 1959 as one of the Ghana 
                        Education Trust Schools. Mr. F.D.K Goka, the Volta Regional Commissioner 
                        for Education at the time, laid the foundation stone. Sixty-six boys 
                        started the school and five teaching staff in 1961 anchored on the 
                        Motto: Consilio et Animis to wit, Wisdom and Courage. Mr. Addo-Yobo 
                        of blessed memory served as the first headmaster of the school. 
                        The school has a total land size of 659 acres which was released...
                    </p>
                    <a href="/history" class="bg-red-900 hover:bg-red-800 text-white rounded-full px-3 py-2 inline-block">Read more</a>
                </div>
            </div>
        </div>
    </section>

    {{-- Motto and Anthem --}}
    <section class="py-16 bg-gray-200 text-center text-gray-900">
        <div class="md:flex max-w-screen-md mx-auto">
            <div class="container flex justify-center items-center mb-12">
                <div>
                    <div class="tracking-widest uppercase font-bold text-2xl text-red-900">Motto</div>
                    <div class="text-2xl sm:text-3xl">Consilio et Animis</div>
                    <div class="italic">(Wisdom and Courage)</div>
                </div>
            </div>
            <div class="container flex flex-col items-center">
                <div class="tracking-widest uppercase font-bold text-2xl text-red-900">&#127932; Anthem 	&#127932;</div>
                <div class="mb-6">
                    Kadjebi Asato Secondary School <br>
                    The great KASEC <br>
                    A giant school in our noble Ghana. <br>
                    Our motto runs in a perfect saying <br>
                    Wisdom and Courage <br>
                    Displayed in the emblem <br>
                    Book and eagle  <br>
                    And the wisdom knot. <br>
                    // Wisdom and courage <br>
                    To be cherished forever <br>
                    As the motto of our great Kasec //
                </div>
                <audio controls>
                    <source src="/audio/kasec-anthem.mpeg" type="audio/mpeg">
                    Your browser does not support the audio element.
                </audio>
            </div>            
        </div>
    </section>

    {{-- Courses Offered --}}
    <section class="py-16" id="courses-offered">
        <div class="container">
            <div class="tracking-widest uppercase text-center text-2xl text-red-900 pb-4">courses offered</div>
            {{-- <div class="grid grid-cols-2 sm:grid-cols-2 md:grid-cols-6 gap-6 mt-4"> --}}
            <div class="flex justify-center flex-wrap mt-4">
                @foreach (config('kasec.courses') as $course)
                    <a href="{{route('courses', ['page' => $course['page']])}}" class="block hover:shadow rounded-lg p-8 w-1/2 sm:w-1/3  text-center">
                        <img src="{{$course['image']}}" alt="excecutive" class="mx-auto rounded-lg mb-2 w-24">
                        <p class="text-red-900 leading-tight">{{$course['name']}}</p>
                    </a>
                @endforeach 
            </div>
        </div>
    </section>

    {{-- Recent Events --}}
    {{-- <section class="py-16 border-t">
        <div class="container">
            <div class="tracking-widest uppercase text-center text-red-900  text-2xl pb-4">recent events</div>
            <div class="sm:flex justify-around mt-4">
                @foreach (config('kasec.news') as $news)
                    <div class="mb-8">
                        <a href="#">
                            <img src="{{$news['image']}}" alt="news" class="border rounded-lg w-full sm:w-48">
                            <p class="text-red-900 font-bold mt-2">{{$news['name']}}</p>
                        </a>
                    </div>
                @endforeach 
            </div>
        </div>
    </section> --}}

    {{-- <section class="container px-4">
        <div class="tracking-widest uppercase text-center text-2xl text-red-900 pb-4">where we are</div>
        <div id="map" class="h-64 border mb-12"></div>
    </section> --}}

    {{-- Alumini section --}}
    <section class="py-16 bg-gray-900 text-white">
        <div class="tracking-widest uppercase text-center text-2xl">alumini</div>
        <div class="container sm:flex justify-center items-center py-16">
            <div class="flex-1 mb-6 sm:mr-12">
                <img src="/images/emblem.jpeg" alt="emblem" class="h-32 sm:h-64 rounded-lg mx-auto sm:mx-0 sm:ml-auto">
            </div>
            <div class="flex-1 text-center sm:text-left">
                <p class="text-3xl uppercase text-yellow-200">kasec old students union</p>
                <p>"The fruits of KASEC have dispersed all over the world filling various departments of human undertakings..."</p>
                <a href="{{route('alumini')}}" class="inline-block px-3 py-2 rounded-full bg-yellow-200 text-red-900 mt-6">read more</a>
            </div>
        </div>
    </section>

    {{-- 60th anniversary --}}
    <section class="py-16 border-t">
        <div class="container">
            <div class="tracking-widest uppercase font-bold text-center text-red-900  text-2xl pb-4">News & Events</div>
            <div class="max-w-screen-md mx-auto mt-4 space-y-5 mb-12">
                <div class="tracking-widest font-bold uppercase text-red-900">60th Anniversary</div>
                @foreach (config('kasec.speeches') as $speech)
                    <div class="flex">
                        <div class="w-1/6">
                            <img src="{{$speech['image']}}" class="h-24 rounded-lg mr-4 object-cover" alt="hod of business dept">
                        </div>
                        <div class="w-5/6 text-red-900 flex flex-col justify-between p-4">
                            <p>{{$speech['title']}}</p> 
                            <a href="{{route('anniversary.speech', ['speech' => $speech['uri'] ])}}" class="text-blue-500">Read more...</a>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="max-w-screen-md mx-auto mt-4 space-y-5 mb-12">
                <div class="tracking-widest font-bold uppercase text-red-900">News</div>
                <div class="flex">
                    {{-- <div class="w-1/6">
                        <img src="{{$speech['image']}}" class="h-24 rounded-lg mr-4 object-cover" alt="hod of business dept">
                    </div> --}}
                    <div class="w-5/6 text-red-900 flex flex-col justify-between p-4">
                        <p class="font-bold">JoyNews</p>
                        <p>Kadjebi-Asato Old Students Union presents face masks to alma mater</p> 
                        <a href="https://www.youtube.com/watch?v=DvFFVhQZMwM&ab_channel=JoyNews" class="text-blue-500">Read more...</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</x-app-layout>
